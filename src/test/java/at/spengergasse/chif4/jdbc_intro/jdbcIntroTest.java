package at.spengergasse.chif4.jdbc_intro;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class jdbcIntroTest {

    private static Connection con;

    @BeforeClass
    public static void setupClass() throws Exception {
        // load driver
        Class.forName("com.mysql.jdbc.Driver");

        // establish connection to db
        con = DriverManager.getConnection("jdbc:mysql://localhost/pos15?user=root");
        con.setAutoCommit(false);

        // create table
        Statement createTableStmt = con.createStatement();
        createTableStmt.execute("CREATE TABLE IF NOT EXISTS t_test (t_id int AUTO_INCREMENT PRIMARY KEY, t_name varchar(20) NULL)");
    }

    @Before
    public void setup() throws Exception {
        Statement truncateTableStmt = con.createStatement();
        truncateTableStmt.execute("TRUNCATE TABLE t_test");
        con.commit();
    }

    @Test
    public void testSelect() throws Exception {
        // given
        String name = "Wien";

        PreparedStatement insertStmt = con.prepareStatement("INSERT INTO t_test(t_name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
        insertStmt.setString(1, name);
        insertStmt.executeUpdate();

        ResultSet rsKeys = insertStmt.getGeneratedKeys();
        int id = -1;
        if (rsKeys.next()) {
            id = rsKeys.getInt(1);
        }

        // when
        PreparedStatement selectStmt = con.prepareStatement("SELECT t_name FROM t_test WHERE t_id = ?");
        selectStmt.setInt(1, id);
        ResultSet rs = selectStmt.executeQuery();

        //then
        if (rs.next()) {
            assertThat(rs.getString(1), is(equalTo(name)));
        }
    }

    @Test
    public void testInsert() throws Exception {
        //given
        String name = "Wien";

        PreparedStatement insertStmt = con.prepareStatement("INSERT INTO t_test(t_name) VALUES (?)");
        insertStmt.setString(1, name);
        insertStmt.executeUpdate();

        name = "Graz";

        PreparedStatement insertStmt2 = con.prepareStatement("INSERT INTO t_test(t_name) VALUES (?)");
        insertStmt.setString(1, name);
        insertStmt.executeUpdate();
        con.commit();

        //when
        PreparedStatement selectStmt = con.prepareStatement("SELECT COUNT(*) FROM t_test");
        ResultSet rs = selectStmt.executeQuery();
        int count = -1;
        if(rs.next()) {
            count = rs.getInt(1);
        }

        //then
        assertThat(count, is(equalTo(2)));
    }

    @Test
    public void testDelete() throws Exception {
        //given
        String name = "Wien";

        PreparedStatement insertStmt = con.prepareStatement("INSERT INTO t_test(t_name) VALUES (?)");
        insertStmt.setString(1, name);
        insertStmt.executeUpdate();

        name = "Graz";

        PreparedStatement insertStmt2 = con.prepareStatement("INSERT INTO t_test(t_name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
        insertStmt.setString(1, name);
        insertStmt.executeUpdate();
        ResultSet rsKeys = insertStmt.getGeneratedKeys();
        int id = -1;
        if (rsKeys.next()) {
            id = rsKeys.getInt(1);
        }
        con.commit();

        //when
        PreparedStatement delelteStmt = con.prepareStatement("DELETE FROM t_test WHERE t_id = ?");
        delelteStmt.setInt(1, id);
        delelteStmt.execute();

        PreparedStatement selectStmt = con.prepareStatement("SELECT COUNT(*) FROM t_test");
        ResultSet rs = selectStmt.executeQuery();
        int count = -1;
        if(rs.next()) {
            count = rs.getInt(1);
        }

        //then
        assertThat(count, is(equalTo(1)));
    }
}
